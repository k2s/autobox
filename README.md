#Autobox

Autobox is a custom Dojo component that provides a textbox within which user inputs can be tokenised (apparently) inline with text. Think of the message address fields of Gmail or Facebook, in which emails or names are tokenised into boxes that can be clicked or backspaced to be removed atomically. Autobox emulates this functionality, but takes it to the next level.

Autobox’s inline tokens are not constrained to the left-most side of the input field. They are truly inline, and you can jump across them using the left and right arrows keys (or Home and End). Each token can also be removed with Delete, Backspace or by clicking on the x icon, merging the contents on either side.

Finally, Autobox extends Dijit’s FilteringSelect, so while typing in an Autobox field, autocomplete suggestions can be provided on the fly, at any point in the text. Selecting an autocomplete option generates a token at that point in the input, preserving the content on either side. These autocomplete suggestions are drawn from a standard Dojo store, which may be making dynamic requests to a server behind the scenes. Suggestions are triggered by the `_startSearchFromInput` method, and can be totally customised. Currently Autobox loosely supports arithmetic equations with alphabetical characters generating autocomplete suggestions for variable names.

##Usage

Using Autobox is easy:

1. Include the autobox module in your code (we recommend adding the Autobox repository as a submodule alongside your custom modules).
2. Update your dojoConfig to include autobox as a package:

        <script type="text/javascript" src="static/dojo/dojo.js"
          data-dojo-config="'async':true, 'parseOnLoad':true, isDebug:true,
               'packages': [
            { name: 'dojo', location: 'dojo' },
            { name: 'dijit', location: 'dijit' },
            { name: 'autobox', location: 'autobox' }
          ]">
        </script>

3. Remember to include `autobox.css` with the rest of your stylesheets!
4. Require `autobox/AutoTextarea` to import the main widget. This can be used much the same was as `dijit/form/Textarea`, either programatically or declaratively.
5. Once instantiated (or in the constructor), manually set values for the store and query properties to determine the source of the autocomplete suggestions (optional). The store is typically a JsonRest store pointing at a REST resource, and query is a map of arguments and values passed along with each request.
6. Customise the autocompleting behaviour in AutoInput (optional).

##Contact

If you have any issues with Autobox or you’d like to discuss developing it further and submitting patches, please contact the MUtopia team at <mutopiamail@gmail.com>, or leave your comments here on the Bitbucket repository. We have an internal issue tracker, but will be watching Bitbucket’s as well. Any help with the development of this module would always be greatly appreciated!

##Known Issues

Although much care has gone into developing Autobox, it was also on a tight schedule, and there are a number of outstanding issues that require some more attention to resolve or workaround:

*   Once the size of the input content exceeds the length of the Autobox, the content is wrapped but not very well. No input is lost, and generally all input remains visible, but behaviour is currently undefined. Some tweaking to the fitLayout method is required to handle this case more correctly.
*   The input spacing for non-monospace fonts is imperfect. This can be corrected in `cropInput` by adjusting for the input’s computed CSS style and measuring the pixel width more accurately with that knowledge.
*   The autocomplete and validation behaviour customisation could be better abstracted away from the underlying FilteringSelect logic.
*   Up and down arrows do not yet work with AutoTextarea.
*   Actions such as cut, copy, paste and drag are not yet supported for tokens.
*   AutoTextBox is not yet implemented. This widget is supposed to act like a Dijit TextBox; that is, scroll rather than wrap when the content exceeds the widget’s width. This should not be too difficult given the existing components.
*   There is potential for numerous widget options, such as locking tokens to the left side (a la Facebook) or truncating long token labels.


##License

Released under the MIT license. See LICENSE.
