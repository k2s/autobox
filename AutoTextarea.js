define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/_base/array',
    'dojo/_base/event',
    'dojo/keys',
    'dojo/on',
    'dojo/when',
    'dojo/aspect',
    'dojo/dom-construct',
    'dojo/dom-style',
    'dojo/dom-geometry',
    'dijit/focus',
    'dojo/store/Memory',
    'autobox/Token',
    'dijit/form/TextBox',
    'autobox/AutoInput',
    'dijit/form/_TextBoxMixin',
    'dijit/layout/ContentPane',
    'dijit/_CssStateMixin'
], function (declare, lang, array, event, keys, on, when, aspect, domConstruct, domStyle, domGeometry, focusUtils, Memory, Token, TextBox, AutoInput, _TextBoxMixin, ContentPane, _CssStateMixin) {
    // Mix in _CssStateMixin to apply textbox-like styles on events (such as hover)
    return declare([ContentPane, _CssStateMixin], {

        // summary:
        //      An autocompleting widget which splices token objects in between FilteringSelect widgets.

        // Add dijitTextBox so the _CssStateMixin handles all the animations
        "class": "autobox autoboxTextarea",
        baseClass: "dijitTextBox",

        // Cached sum of the widths of the tokens to simplify resizing
        tokensWidth: 0,
        // The regex use to match an item reference.
        itemRegex: null,

        // The datastore backing the autocomplete fields
        store: null,
        // The query to pass with all store requests
        query: null,

        constructor: function (options) {
            this.query = {};
            this.itemRegex = /\$(\d+)/gmi;
            // Define internal array
            // Ordered list of the input field widgets
            this.inputWidgets = [];
            // Ordered list of the token widget
            this.tokens = [];
        },

        postCreate: function () {
            this.inherited(arguments);

            // Store the size properties of this container widget
            this.containerWidth = this._calculateWidth();

            // A store is required to create child inputs, so if one wasn't passed in the constructor, use a placeholder
            if (!this.store) {
                this.store = new Memory({});
            }

            // Place the initial input field
            var newInput = this.buildInput();
            this.inputWidgets.push(newInput);
            domConstruct.place(newInput.domNode, this.containerNode, "last");
            this.fitLast();

            // After creating or deleting a token, resize the children to fit the widget
            aspect.after(this, "tokenise", this.fitLayout);
            aspect.after(this, "onTokenDelete", this.fitLayout);

            // An ugly hack to make this work in a dgrid editor, since it takes a while to be attached to the DOM
            // FIXME Hook the initial DOM sizing into a more appropriate method (startup doesn't help)
            var timeOut = 3000;
            var freq = 10;
            var poll = setInterval(lang.hitch(this, function () {
                this.containerWidth = this._calculateWidth();
                timeOut -= freq;
                // If container is wider than 1px or we time out.
                // NOTE: (otherwise >50% CPU issues occur - see Chrome Profiler)
                if (this.containerWidth > 1 || timeOut <= 0) {
                    clearInterval(poll);
                    if (timeOut <= 0) {
                        console.debug('_calculateWidth took too long! Width is', this.containerWidth);
                    } else if (this.containerWidth > 1) {
                        this.fitLayout();
                    }
                }
            }), freq);
        },

        _calculateWidth: function () {
            // summary:
            //      Method of calculating the available width for child widgets.

            var containerStyle = domStyle.getComputedStyle(this.containerNode);
            // FIXME for some reason, this is giving -2 for height and width...
            return domGeometry.getContentBox(this.containerNode, containerStyle).w;
        },

        _onInputKeydown: function (input, evt) {
            // summary:
            //      Handle a key press within one of the input fields
            //      If one of the interesting conditions matches, the event is stopped, since all
            //      widget-related behaviour is fully specified here.
            var widgetIndex = array.indexOf(this.inputWidgets, input);
            var inputLength = input.get("value").length;
            var key = evt.charCode || evt.keyCode;
            switch (key) {
                case keys.LEFT_ARROW:
                    if (widgetIndex > 0 && input.getCaretPos() == 0) {
                        // Jump to the previous widget
                        this.focusInput(this.inputWidgets[widgetIndex - 1], "end");
                        event.stop(evt);
                    }
                    break;
                case keys.RIGHT_ARROW:
                    if (widgetIndex < this.inputWidgets.length - 1 && input.getCaretPos() == inputLength) {
                        // Jump to the next widget
                        this.focusInput(this.inputWidgets[widgetIndex + 1], "start");
                        event.stop(evt);
                    }
                    break;
                case keys.BACKSPACE:
                    if (widgetIndex > 0 && input.getCaretPos() == 0) {
                        // Delete the previous token
                        this.tokens[widgetIndex - 1].onDelete();
                        // Merging the inputs will be taken care of in onTokenDelete
                        event.stop(evt);
                    }
                    break;
                case keys.DELETE:
                    if (widgetIndex < this.inputWidgets.length - 1 && input.getCaretPos() == inputLength) {
                        // Delete the following token
                        this.tokens[widgetIndex].onDelete();
                        // Merging the inputs will be taken care of in onTokenDelete
                        event.stop(evt);
                    }
                    break;
                case keys.HOME:
                    this.focusInput(this.inputWidgets[0], "start");
                    event.stop(evt);
                    break;
                case keys.END:
                    this.focusInput(this.inputWidgets[this.inputWidgets.length - 1], "end");
                    event.stop(evt);
                    break;
                case keys.TAB:
                    break;
                default:
                    // No match, no action, but also no event stopping
                    return;
            }
        },

        buildInput: function (/*String*/ text, /*Boolean?*/ append) {
            // summary:
            //      Construct a new input field with the necessary properties and bindings.
            //      The caller is required to store the reference and DOM node unless append is truthy.
            // text: String
            //      The value to set on the input.
            // append: Boolean
            //      Whether to append the new input to the input container automatically (for convenience).
            text = text || null;
            // Create the widget
            // Note: This AutoTextarea must be attached to the DOM before creating child inputs
            var newInput = new AutoInput({
                name: "state",
                store: this.store,
                query: this.query,
                searchAttr: "name",
                "class": "autoboxInput",
                autoComplete: true
            });
            // Ensure that the textarea's query is used as necessary
            lang.delegate(newInput.query, this.query);
            if (text) {
                newInput.set("value", text);
            }

            // Resize on when the contents of the input change
            // TODO Bind to a more explicit event for keystrokes
            on(newInput.focusNode, "input", lang.hitch(this, function () {
                this.cropInput(newInput, true);
            }));
            aspect.after(newInput, "_autoCompleteText", lang.hitch(this, function () {
                this.cropInput(newInput, true);
            }));

            // Repeat changes form children to be picked up by parents
            on(newInput.focusNode, "blur", lang.hitch(this, function () {
                this.onBlur();
            }));
            on(newInput.focusNode, "change", lang.hitch(this, function () {
                this.onChange();
            }));

            // Listen for key presses to handle navigation
            on(newInput.focusNode, "keydown", lang.hitch(this, function (evt) {
                this._onInputKeydown(newInput, evt);
            }));

            // Listen for tokenisation events from this input
            aspect.after(newInput, "tokenise", lang.hitch(this, function (tokenText, fullText, start, end) {
                this.tokenise(tokenText, fullText, start, end, newInput);
            }), true);

            if (append) {
                this.inputWidgets.push(newInput);
                domConstruct.place(newInput.domNode, this.containerNode, "last");
            }

            return newInput;
        },

        buildToken: function (id, text, append, ref) {
            // summary:
            //      Construct a new token item with the given properties.
            //      The caller is required to store the reference and DOM node unless append is specified.
            // id: Integer
            //      The ID of the item backing the token.
            // text: String
            //      The text to display on the token.
            // append: Object
            //      Whether to append the new token to the input container automatically (for convenience).
            //      If simply true, the token will be appended to the end of the container.
            //      If an input widget, the token will be appended after it.
            //      Otherwise, the token will not be placed or stored, and the caller is responsible for it.
            // ref: Object
            //      The original reference object generated from the value of this textarea. Can be null if tokenised.
            var token = this.createToken(id, text, ref);

            aspect.after(token, 'onChange', lang.hitch(this, this.onChange));

            // Append the end
            if (append === true) {
                this.tokens.push(token);
                domConstruct.place(token.domNode, this.containerNode, "last");
            }
            // Assume append is an input widget
            else if (append) {
                var index = array.indexOf(this.inputWidgets, append);
                if (index !== -1) {
                    this.tokens.splice(index + 1, 0, token);
                    domConstruct.place(token.domNode, append.domNode, "after");
                }
            }
            return token;
        },

        createToken: function (id, label, ref) {
            // summary:
            //      Constructs an instance of a Token.
            // id: Integer
            //      The ID of the item backing the token.
            // value: String
            //      The value to display on the token.
            // ref: Object
            //      The original reference object generated from the value of this textarea. Can be null if tokenised.
            return new Token({itemId: id, label: label, parentBox: this});
        },

        tokenise: function (item, fullText, start, end, inputWidget) {
            // summary:
            //      Convert a completed item into an item widget.
            if (!item || !inputWidget) {
                return;
            }
            var inputIndex = array.indexOf(this.inputWidgets, inputWidget);

            // Split out a new input for the left of the token with any leading text
            var newInput = this.buildInput(fullText.substring(0, start));
            this.inputWidgets.splice(inputIndex, 0, newInput);
            domConstruct.place(newInput.domNode, inputWidget.domNode, "before");

            // Create and insert the token
            var token = this.buildToken(item.id, item.name, false);
            this.tokens.splice(inputIndex, 0, token);
            domConstruct.place(token.domNode, inputWidget.domNode, "before");

            token.cacheStyle = domStyle.getComputedStyle(token.domNode);
            token.cacheWidth = domGeometry.getMarginBox(token.domNode, token.cacheStyle).w;
            this.tokensWidth += token.cacheWidth;

            // Leave any trailing text in the original input
            inputWidget.set("value", inputWidget.get("value").substr(end));
            // FIXME Doesn't go to the start, don't know why
            this.focusInput(inputWidget, "start");
            // Validate the new content
            inputWidget.validate();
            // Call the hook
            this.onChange();
        },

        focusInput: function (input, index) {
            // summary:
            //      Convenience method for focusing on an input widget.
            input.focus();
            input.setCaretPos(index);
        },

        cropInput: function (input, atomic) {
            // summary:
            //      Set the width of the given input widget to the width of its contents.
            // atomic: Boolean
            //      Whether the crop is being performed on a single input (true) or as part
            //      of a larger layout update (false).
            atomic = atomic || false;
            input.focusNode.size = Math.max(1, input.get("value").length);
            if (atomic) {
                this.fitLast();
            }
        },

        fitLayout: function () {
            // summary:
            //      Manually adjust the width of each child widget to fit seamlessly.
            //      Must be called after the list of children is correct (after create/delete).

            // Fit each individual textbox
            array.forEach(this.inputWidgets, lang.hitch(this, function (input) {
                this.cropInput(input);
            }));
            // Adjust the last box to fit
            this.fitLast();
        },

        fitLast: function () {
            // summary:
            //      Resize the last input field to take up the remaining space left by the
            //      previous inputs and tokens.

            if (this.inputWidgets.length) {
                // Add up the total width of the previous children
                var total = this.tokensWidth;
                array.forEach(this.inputWidgets, lang.hitch(this, function (input, i) {
                    if (i < this.inputWidgets.length - 1) {
                        total += domStyle.get(input.domNode, "width");
                    }
                }));
                // Get the last input
                var last = this.inputWidgets[this.inputWidgets.length - 1];
                // Its width should be the difference between the container and previous elements.
                var width = Math.max(1, this.containerWidth - total) + "px";
                // Set the width
                domStyle.set(last.domNode, "width", width);
            }
        },

        removeInput: function (input, index) {
            // summary:
            //      Removes the input from the widget (reference and DOM node).
            // input:
            //      The input widget to remove
            // index:
            //      The index of the widget in this.inputWidgets, if known.
            if (index === null || typeof index === "undefined") {
                index = array.indexOf(this.inputWidgets, input);
            }
            input.destroy();
            this.inputWidgets.splice(index, 1);
        },

        onTokenDelete: function (token) {
            // summary:
            //      Handle the deletion of a token.

            // Remove the token
            this.tokensWidth -= token.cacheWidth;
            var tokenIndex = array.indexOf(this.tokens, token);
            this.tokens.splice(tokenIndex, 1);

            // Merge the (n+1)th input into the nth
            var left = this.inputWidgets[tokenIndex];
            var right = this.inputWidgets[tokenIndex + 1];
            // Save the left length for cursor placement
            var leftLength = left.get("value").length;
            // Merge the text
            left.set("value", left.get("value") + right.get("value"));
            // Remove the right input
            this.removeInput(right, tokenIndex + 1);
            // Update the focus to what it would have been
            this.focusInput(left, leftLength);
        },

        reset: function (/*Boolean?*/ total) {
            // summary:
            //      Clear the auto-complete widget of all children except an empty input.
            // total:
            //      Destory the original input as well.

            total = total || false;

            // Remove all tokens
            array.forEach(array.filter(this.tokens, function () {
                return true;
            }), lang.hitch(this, function (token) {
                token.onDelete();
            }));

            // Remove all inputs but the first (must be after tokens are removed)
            array.forEach(array.filter(this.inputWidgets, function () {
                return true;
            }), lang.hitch(this, function (input, i) {
                if (total || i > 0) {
                    this.removeInput(input);
                }
            }));

            if (!total) {
                // Reset the first input
                this.inputWidgets = [this.inputWidgets[0]];
                this.inputWidgets[0].set("value", "");
                this.fitLast();
            } else {
                this.inputWidgets = [];
            }
        },

//        tokenToString: function (token) {
//            // summary:
//            //      Method for customising the serialisation of tokens.
//            return this.itemPrefix + token.itemId;
//        },

//        stringToTokens: function (value) {
//            // summary:
//            //      Method for customising the parsing of a string to tokens.
//            return value.split(/\$\d+/);
//        },

        toString: function () {
            // summary:
            //      Sequentially serialise the contents of this autobox into a string.
            var output = "";
            if (this.inputWidgets.length) {
                output = this.inputWidgets[0].get("value");
                array.forEach(this.tokens, lang.hitch(this, lang.hitch(this, function (token, i) {
                    output += token.get('value') + this.inputWidgets[i + 1].get("value");
                })));
            }
            return output;
        },

        onBlur: function () {
            // summary:
            //      Hook to handle the event of blurring out of any input.
        },

        onChange: function () {
            // summary:
            //      Hook to handle the event of changing the content of any input.
        },

        findRefs: function (/*String*/ text, map) {
            // summary:
            //      Find all start-end index pairs representing references.
            // text:
            //      The text to search within.
            // map:
            //      A function to modify the references as they are added.
            // return:
            //      Array of {start, end, id} maps.

            map = typeof map != 'undefined' ? map : lang.hitch(this, this._refMatch);
            var refs = [];
            var match;
            while (match = this.itemRegex.exec(text))  {
                var ref = map ? map(match) : match;
                refs.push(ref);
            }
            return refs;
        },

        _refMatch: function (match) {
            var ref = {};
            ref.id = parseInt(match[1]);
            ref.start = match.index;
            ref.end = ref.start + match[0].length;
            return ref;
        },

        findRefsInStore: function (refs, callback) {
            // summary:
            //      Retrieves items from the store using reference ids.
            // refs: Array
            //      The references.
            // callback:
            //      A function to call after all items are retrieved.
            //      Arguments passed are the original refs and items arrays.
            var remaining = refs.length;
            var items = {};
            refs.forEach(lang.hitch(this, function (ref) {
                if (!(ref.id in items)) {
                    // TODO must be int here
                    when(this.store.get(ref.id), lang.hitch(this, function (item) {
                        items[ref.id] = item;
                        if (--remaining == 0) {
                            // All items received from store
                            callback(refs, items);
                        }
                    }));
                }
            }));
        },

        _getValueAttr: function () {
            // summary:
            //      Interface for widget.get("value").
            // returns:
            //      A stringified version of the input expression, or null if empty.
            return this.toString() || null;
        },

        _setValueAttr: function (value) {
            // summary:
            //      Set up the auto-complete widget given a serialised string.

            // Clear any existing content and start from scratch
            // If there's no content, we're done
            if (!value) {
                this.reset();
                return;
            }
            // If there is content, wipe out the original input as well
            else {
                this.reset(true);
            }

            var refs = this.findRefs(value);

            // Request data from store before creating tokens
            // NOTE: otherwise we cannot guarantee tokens are created in correct order and this leads to tokens swapping order
            if (refs.length > 0) {
                this.findRefsInStore(refs, lang.hitch(this, function (refs, items) {
                    this.__setValueAttr(value, refs, items);
                }));
            } else {
                this.__setValueAttr(value, [], []);
            }
        },

        __setValueAttr: function (value, refs, items) {
            var lastEnd = 0;
            refs.forEach(lang.hitch(this, function (ref) {
                var item = items[ref.id];
                // Create the left widget
                var newInput = this.buildInput(value.substring(lastEnd, ref.start), true);
                // Create the token
                this.buildToken(item.id, item.name, newInput, ref);
                // Keep track of where we're up to
                lastEnd = ref.end;
            }));
            // Add the final right-most input
            this.buildInput(value.substr(lastEnd), true);
            // After all is done, make sure everything fits
            this.fitLayout();
        },

        _setStoreAttr: function (store) {
            this.store = store;
            array.forEach(this.inputWidgets, function (input) {
                input.set("store", store);
            });
        },

        _setQueryAttr: function (query) {
            this.query = query;
            array.forEach(this.inputWidgets, function (input) {
                input.set("query", query);
            });
        },

        _setPlaceholderAttr: function (text) {
            // summary:
            //      Allow the placeholder text to be set if there's only a single textbox.
            if (this.inputWidgets.length === 1) {
                this.inputWidgets[0].set("placeholder", text);
            }
        }
    });
});
