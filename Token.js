define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'dojo/on',
    'dojo/query',
    'dojo/dom-construct',
    'dojo/dom-class',
    'dijit/layout/ContentPane',
    "dojo/text!./templates/TokenContent.html",
    // Required
    'utility/String'
], function (declare, lang, on, query, domConstruct, domClass, ContentPane, template) {
    return declare([ContentPane], {

        // summary:
        //      Inspired by http://www.codeproject.com/Articles/183498/Fancy-Facebook-Style-TextboxList

        content: template,
        baseClass: "autoboxToken",
        _hideButtonCSS: 'autoboxButtonHidden',
        _buttonCSS: 'autoboxButton',

        // The ID of the underlying item
        itemId: null,
        // The character used as a prefix to designate an object ID
        itemPrefix: "$",

        // The text to display
        label: null,
        parentBox: null,

        // DOM elements
        spanLabel: null,
        btnDelete: null,

        postCreate: function () {
            this.inherited(arguments);
            this.spanLabel = query('.autoboxLabel', this.domNode)[0];
            this.autoboxButtons = query('.autoboxButtons', this.domNode)[0];
            this._initButtons();
            this.set('label', this.label);
        },

        _initButtons: function () {
            this.btnDelete = this.addButton(this.createButton('Delete'));
            on(this.btnDelete, 'click', lang.hitch(this, this.onDelete));
        },

        addButton: function (button, pos) {
            domConstruct.place(button, this.autoboxButtons, pos);
            return button;
        },

        createButton: function (name, baseCls) {
            baseCls = baseCls || this._buttonCSS;
            name = name.toTitleCase();
            return domConstruct.toDom('<span data-qid="btn' + name + '" role="button" class="' + baseCls + ' autobox' + name + '"></span>');
        },

        setButtonVisible: function (button, visible) {
            visible ? this.showButton(button) : this.hideButton(button);
        },

        hideButton: function (button) {
            domClass.add(button, this._hideButtonCSS);
        },

        showButton: function (button) {
            domClass.remove(button, this._hideButtonCSS);
        },

        _getLabelAttr: function () {
            return this.label;
        },

        _setLabelAttr: function (label) {
            this.label = label;
            if (this.spanLabel) {
                this.spanLabel.innerHTML = label;
            }
            this.onChange();
        },

        _setValueAttr: function (value) {
            // TODO
        },

        _getValueAttr: function () {
            // summary:
            //      Method for customising the serialisation of tokens.
            return this.itemPrefix + this.itemId;
        },

        onDelete: function () {
            // summary:
            //      Destroy the widget and its DOM node.
            this.parentBox.onTokenDelete(this);
            this.onChange();
            this.destroy();
        },

        onChange: function () {

        }
    });
    /* end declare() */
});
/* end define() */